import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Booked from "./pages/Booked";
import Booking from "./pages/Booking";
import Layout from "./pages/Layout";
import Login from "./pages/Login";
import AddDetails from "./pages/AddDetails"
import Register from "./pages/Register";

const app = document.getElementById('app');

ReactDOM.render(
  <Router history={hashHistory}>

    <Route path="/" component={Layout}>
    
      <IndexRoute component={Booking}></IndexRoute>

      <Route path="/booked" name="booked" component={Booked}></Route>

      <Route path="/addDetails" name="addDetails" component={AddDetails}></Route>

      <Route path="/register" name="register" component={Register}></Route>

      <Route path="/login" name="login" component={Login}></Route>
    </Route>
  </Router>,
app);
