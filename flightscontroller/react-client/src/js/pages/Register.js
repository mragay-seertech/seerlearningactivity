import React, { Component } from 'react';
import {register} from './UserFunctions';
import axios from 'axios';

class Register extends Component {
    state = { 
        username: '',
        password: ''
     };
    handleChange = event => {
        this.setState({ username: event.target.value })
        
    }
    passChange = event => {
        this.setState({ password: event.target.value })
    }
    handleSubmit = event => {
        event.preventDefault();
       
    axios.post('http://127.0.0.1:5000/user/register', { username:this.state.username, password:this.state.password  })
         .then(res => {
             console.log(res);
             console.log("added")
             console.log(res.data);
         })
         .catch(error => {
            console.log(error.response)
        })
     }; 
    render() { 
        return ( 
            <div className="container">
                <div className="column is-9  is-offset-2">
                    <div className = "box">
                        <form  onSubmit = {this.handleSubmit}>   
                              <label htmlFor="username">Username</label>
                                <input type="text" 
                                    className = "input  is-primary" 
                                       name= "username" 
                                       placeholder ="Enter Username"
                                       onChange = {this.handleChange}/>            
                                    <br/>
                                    <br/>            
                                     <label htmlFor="password">Password</label>
                                     <input type="password" 
                                       className = "input is-primary" 
                                       name= "password" 
                                       placeholder ="Enter password"
                                       onChange = {this.passChange}/>
                                    <br/>
                                    <br/>
                                    <input type= "submit" className="button is-block is-primary is-fullwidth" value="Add this User"/>
                                </form>
                            </div>
                        </div>
                    </div>
         );
    }
}
export default Register;