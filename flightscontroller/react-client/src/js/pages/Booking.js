import React from "react";
import axios from 'axios';
//import Article from "../components/Article";

export default class Booking extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      items: [],
      isLoaded: false,
    }
  }
  componentDidMount(){
    fetch('http://127.0.0.1:5000/user/details')
      .then(res => res.json())
      .then(json => {
          this.setState({
            isLoaded: true,
            items: json,
          })
      });
  }
  state = {
    origin: '',
    destination: '',
    depart_date: '',
    depart_time: '',
    plane_name: '',
    travel_class: ''
}

oriChange = event => {
    this.setState({ origin: event.target.value })
    
}
destChange = event => {
    this.setState({ destination: event.target.value })
    
}
deptDateChange = event => {
  this.setState({ depart_date: event.target.value })
  
}
deptChange = event => {
    this.setState({ depart_time: event.target.value })
    
}
planeChange = event => {
    this.setState({ carrier: event.target.value })
    
}
travelChange = event => {
    this.setState({ travel_class: event.target.value })
    
}
onSubmit = event => {
    event.preventDefault();

axios.post('http://127.0.0.1:5000/user/booking', {  origin:this.state.origin, destination:this.state.destination, depart_date:this.state.depart_date, depart_time:this.state.depart_time, carrier:this.state.carrier, travel_class:this.state.travel_class  })
    .then(res => {
        console.log(res);
        console.log("added")
        console.log(res.data);
    })
    .catch(error => {
    console.log(error.response)
    })
};
  render() {
    var{ isLoaded, items} = this.state;
    if(!isLoaded){
      return <div className="subtitle">
              <br/><br/>
              Unable to Load Flight Details..
            </div>;
  }else{
        return(
        <div class="container column is-9  is-offset-2">
            <br/>
            <form onSubmit = {this.onSubmit}>
            <table className="table">
              <tr>
                <td className="subtitle">Origin:</td>
                <td><div className="select is-primary is-fullwidth">
                <select name = "origin"  onChange = {this.oriChange}>
                <option></option>
                {items.map(item =>(
                  <option>{item.origin}</option>
                     ))}
                </select></div></td>

                <td className="subtitle">Destination:</td>
                <td><div className="select is-primary is-fullwidth">
                <select name = "destination"  onChange = {this.destChange}>
                <option></option>
                {items.map(item =>(
                  <option>{item.destination}</option>
                     ))}
                </select></div></td>
              </tr>
              <tbody><td className="subtitle">Departure Date:</td>
              <td><input type="date" className="input is-primary is-fullwidth" name="depart_date"  onChange = {this.deptDateChange}/></td>

              <td className="subtitle">Departure Time:</td>
              <td><div className="select is-primary is-fullwidth">
                <select name="depart_time"  onChange = {this.deptChange}>
                <option></option>
                {items.map(item =>(
                  <option>{item.depart_time}</option>
                     ))}
                </select></div></td>

              </tbody>
              <tbody><td className="subtitle">Plane Name:</td>
              <td ><div className="select is-primary is-fullwidth">
                <select name = "carrier"  onChange = {this.planeChange}>
                <option></option>
                {items.map(item =>(
                  <option>{item.carrier}</option>
                     ))}
                </select></div></td>
              <td className="subtitle">Travel Class:</td>
              <td><div className="select is-primary is-fullwidth">
                <select name="travel_class"  onChange = {this.travelChange}>
                <option></option>
                {items.map(item =>(
                  <option>{item.travel_class}</option>
                     ))}
                </select></div></td>
              </tbody>
              <tbody><td/><td/><td/><td><input type="submit" className="button is-primary is-medium is-fullwidth" value = "Reserve Flight"/></td></tbody>
            </table>
            </form>
            <br/><br/><br/> 
        </div>
    );
  }
 }
}
