import React from "react";

//import Article from "../components/Article";

export default class Booked extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      items: [],
      isLoaded: false,
    }
  }
  componentDidMount(){
    fetch('http://127.0.0.1:5000/user/reserved')
      .then(res => res.json())
      .then(json => {
          this.setState({
            isLoaded: true,
            items: json,
          })
      });
  }
  render() {
    var{ isLoaded, items} = this.state;
    if(!isLoaded){
        return <div className="subtitle">
                <br/><br/>
                Loading..
              </div>;
    }else{
          return(
            <div className="container column is-10  is-offset-1 ">
            <br/><br/>
            <h1 align = 'Center' className='subtitle'>Booked Flights</h1>
            <br/>
                <table className='table' align = "center">
                  <tr>
                    <th>Origin</th>
                    <th>Destination</th>
                    <th>Depart Date</th>
                    <th>Depart Time</th>
                    <th>Plane Name</th>
                    <th>Travel Class</th>
                  </tr>
                    {items.map(item =>(
                      <tr>
                        <td>{item.origin}</td>
                        <td>{item.destination}</td>
                        <td>{item.depart_date}</td>
                        <td>{item.depart_time}</td>
                        <td>{item.carrier}</td>
                        <td>{item.travel_class}</td>
                      </tr>
                    ))}
                  <tr>
                  </tr>
                 </table>
                 <br/><br/><br/>
            </div>
          )
    }
  }
}
