import React from 'react';
import axios from 'axios';

export default class AddDetails extends React.Component {
   state = {
        origin: '',
        destination: '',
        depart_time: '',
        plane_name: '',
        travel_class: ''
    }

    oriChange = event => {
        this.setState({ origin: event.target.value })
        
    }
    destChange = event => {
        this.setState({ destination: event.target.value })
        
    }
    deptChange = event => {
        this.setState({ depart_time: event.target.value })
        
    }
    planeChange = event => {
        this.setState({ plane_name: event.target.value })
        
    }
    travelChange = event => {
        this.setState({ travel_class: event.target.value })
        
    }
    onSubmit = event => {
        event.preventDefault();

    axios.post('http://127.0.0.1:5000/user/add_details', {  origin:this.state.origin, destination:this.state.destination, depart_time:this.state.depart_time, plane_name:this.state.plane_name, travel_class:this.state.travel_class  })
        .then(res => {
            console.log(res);
            console.log("added")
            console.log(res.data);
        })
        .catch(error => {
        console.log(error.response)
        })
    };

    //register(newUser).then( res => {
    //    if(!res.error){
    //        this.props.history.push('/addDetails')
    //    }
    // })

    render() {
      return (
        <div className="container column is-9  is-offset-2">
  
          <br/><br/>
            <form onSubmit = {this.onSubmit}>
              <table className="table">
                  <tr>
                    <td className="subtitle">Origin:</td>
                    <td><input type="text" 
                                    className = "input  is-primary" 
                                       name= "username" 
                                       placeholder ="Enter Origin"
                                       onChange = {this.oriChange}/>  </td>

                    <td className="subtitle">Destination:</td>
                    <td><input type="text" 
                                    className = "input  is-primary" 
                                       name= "username" 
                                       placeholder ="Enter Username"
                                       onChange = {this.destChange}/>  </td>
                  </tr>
                  <tbody>
                  <td className="subtitle">Departure Time:</td>
                  <td><input type="text" 
                                    className = "input  is-primary" 
                                       name= "username" 
                                       placeholder ="Enter Username"
                                       onChange = {this.deptChange}/>  </td>

                  <td className="subtitle">Plane Name:</td>
                  <td ><input type="text" 
                                    className = "input  is-primary" 
                                       name= "username" 
                                       placeholder ="Enter Username"
                                       onChange = {this.planeChange}/>  </td>

                  </tbody>
                  <tbody>
                
                  <td className="subtitle">Travel Class:</td>
                  <td><input type="text" 
                                    className = "input  is-primary" 
                                       name= "username" 
                                       placeholder ="Enter Username"
                                       onChange = {this.travelChange}/>  </td>

                  <td/><td><input type="submit" className="button is-primary is-medium is-fullwidth"  value = "Add to Details"/></td>
                  </tbody>
                </table>
                </form>
            <br/><br/><br/>
        </div>
      );
    }
}