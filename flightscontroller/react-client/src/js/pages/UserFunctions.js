import axios  from 'axios';

export const register = newUser => {
    return axios
        .post('user/register', {
            username: newUser.username,
            password: newUser.password
        })
        .then(res => {
            console.log(res)
        })
}


export const login = user => {
    return axios 
        .post('user/login',{
            username: user.username,
            password: user.password
        })
        .then(response => {
            localStorage.setItem('usertokern', response.data.token)
            return response.data
        })
        .catch(err => {
            console.log(err)
        })

}