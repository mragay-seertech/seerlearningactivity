import React from "react";
import {login} from "./UserFunctions";


export default class Login extends React.Component {
  constructor(){
    super()
    this.state = {
        username: '',
        password: ''
    }
    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
}
onChange(e){
    this.setState({[e.target.name]: e.target.value})
}
onSubmit(e){
    e.preventDefault()

    const user = {
        username: this.state.username,
        password: this.state.password 
    }

    login(user).then( res => {
        if(!res.error){
            this.props.history.push('/')
        }
    })
  }
  render() {
    console.log("login");
    return (
      <div className="container column is-9  is-offset-2">
      <form noValidate onSubmit={this.onSubmit} >
            <label className="subtitle">Username:</label>
              <input className="input" type="text"
                name= "username" 
                placeholder ="Enter Username"
                value = {this.state.username}
                onChange = {this.onChange}/>    
            <br/>
            <br/>
            <label className="subtitle">Password:</label>
            <input className="input" type="password"
                name= "password" 
                placeholder ="Enter password"
                value = {this.state.password}
                onChange = {this.onChange}/> 
            <br/>
            <br/>
            <p className="control">
              <input className="button is-primary is-medium is-fullwidth" type = "submit" value="Login"></input>
            </p>
              <br/>
              <br/>
        </form>
      </div>
    );
  }
}
