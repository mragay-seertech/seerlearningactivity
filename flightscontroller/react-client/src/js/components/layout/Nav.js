import React from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    const {location} = this.props;
    const featuredClass = location.pathname === "/" ? "active" : "";
    const bookedClass = location.pathname.match(/^\/booked/) ? "active" : "";
    const loginClass = location.pathname.match(/^\/login/) ? "active" : "";
    const addDetailsClass = location.pathname.match(/^\/addDetails/) ? "active" : "";
    const registerClass = location.pathname.match(/^\/register/) ? "active" : "";


    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div class="container">
            <div class="navbar-start">
              <div class={featuredClass}/>
                <IndexLink to="/" onClick={this.toggleCollapse.bind(this)} className="navbar-item">Booking Screen</IndexLink>
             
              <div class={bookedClass}/>
                <Link to="booked" onClick={this.toggleCollapse.bind(this)} className="navbar-item">Booked Flights</Link>

              <div class={addDetailsClass}/>
                <Link to="addDetails" onClick={this.toggleCollapse.bind(this)} className="navbar-item">Add Flights Detail</Link>  

              <div class={registerClass}/>
                <Link to="register" onClick={this.toggleCollapse.bind(this)} className="navbar-item">Add User</Link>  
              
              <div class={loginClass}/>
                <Link to="login" onClick={this.toggleCollapse.bind(this)} className="navbar-item">Login</Link>          
            </div>
            </div>
      </nav>
    );
  }
}
