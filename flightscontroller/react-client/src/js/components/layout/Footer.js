import React from "react";


export default class Footer extends React.Component {
  render() {
    return (
      <footer>
        <div class="row">
          <div class="col-lg-12 column is-9  is-offset-2">
            <p>Copyright &copy; M.A.Ragay</p>
          </div>
        </div>
      </footer>
    );
  }
}
