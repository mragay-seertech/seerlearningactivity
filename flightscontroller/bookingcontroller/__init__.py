from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from  flask_login import LoginManager
#import psycopg2

app = Flask(__name__)
app.config['SECRET_KEY'] ='68debe4053d71991441e849ab8fd7e0d'
app.config['SQLALCHEMY_DATABASE_URI']='postgresql+psycopg2://postgres:seer@127.0.0.1:8081/seertech_activity'
db = SQLAlchemy(app)
bcryp=Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'

from bookingcontroller import routes


def create_table():
    db.create_all();
