
from bookingcontroller import db, login_manager
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return Admin.query.get(int(user_id))

class Admin(db.Model, UserMixin):
    admin_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100),unique=True, nullable= False)
    password = db.Column(db.String(100), nullable=False)

    def __init(self, username, password):
        self.username = username
        self.passworf = Password

    def __repr__(self):
        return f"User('{self.admin_id}','{self.username}', '{self.password}')"


class booked(db.Model):

    flight_id = db.Column(db.Integer, nullable=False, primary_key=True)
    origin = db.Column(db.String(50), nullable=False)
    destination = db.Column(db.String(50), nullable=False)
    depart_date = db.Column(db.DateTime(20), nullable=False)
    depart_time = db.Column(db.String(20), nullable=False)
    carrier = db.Column(db.String(50), nullable=False)
    travel_class = db.Column(db.String(30), nullable=False)


    def __init__(self, origin, destination, depart_date, depart_time, carrier, travel_class):
        self.origin =origin
        self.destination = destination
        self.depart_date = depart_date
        self.depart_time = depart_time
        self.carrier = carrier
        self.travel_class = travel_class


    def __repr__(self):
        return f"User('{self.origin}','{self.destination}','{self.depart_date}','{self.depart_time}','{self.carrier}','{self.travel_class}')"



class flights(db.Model):
    detail_id  = db.Column(db.Integer, nullable=False, primary_key=True)
    origin = db.Column(db.String(100), nullable=False)
    destination = db.Column(db.String(100), nullable=False)
    depart_time = db.Column(db.String(100), nullable=False)
    plane_name = db.Column(db.String(100), nullable=False)
    travel_class = db.Column(db.String(100), nullable=False)

    def __init__(self, origin, destination, depart_time, plane_name, travel_class):
        self.origin =origin
        self.destination = destination
        self.depart_time = depart_time
        self.plane_name = plane_name
        self.travel_class = travel_class

    def __repr__(self):
        return f"User('{self.origin}','{self.destination}','{self.depart_time}','{self.plane_name}','{self.travel_class}')"
