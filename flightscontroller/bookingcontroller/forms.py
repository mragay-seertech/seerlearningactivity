from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField, DateField
from wtforms.validators import DataRequired, Length

class LoginForm(FlaskForm):
    username = StringField('Username',
                        validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
    cancel = SubmitField('Cancel')

class registerForm(FlaskForm):
    username = StringField('Username',
                        validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Register')

class BookingForm(FlaskForm):
    origin = StringField('Flying from:', validators=[DataRequired(), Length(min=5, max=50)])
    destination = StringField('Flying to:', validators=[DataRequired(), Length(min=5,max=50)])
    depart_date = DateField('Depart date:', validators=[DataRequired()])
    depart_time = StringField('Time:',validators=[DataRequired()])
    carrier = StringField('Flight Carrier:', validators=[DataRequired()])
    travel_class = SelectField('Travel class:', validators=[DataRequired()])
    adultsCount = SelectField('Travel class:', validators=[DataRequired()])
    childCount = SelectField('Travel class:', validators=[DataRequired()])
