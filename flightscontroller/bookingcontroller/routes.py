
from flask import Flask, render_template, url_for, flash, redirect, jsonify, request, json
from bookingcontroller import app,db
import psycopg2
import json
import collections
import datetime
from bookingcontroller.forms import LoginForm, BookingForm, registerForm
from bookingcontroller.models import Admin, booked, flights
from flask_login import login_user
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_jwt_extended import JWTManager, create_access_token
bcrypt = Bcrypt()
jwt =JWTManager(app)


##checked
@app.route("/")
@app.route("/user/booking", methods=['GET','POST'] )
def booking():

    org = request.get_json()['origin']
    dest = request.get_json()['destination']
    d_date = request.get_json()['depart_date']
    d_time = request.get_json()['depart_time']
    car = request.get_json()['carrier']
    t_class = request.get_json()['travel_class']

    book = booked(origin=org, destination=dest, depart_date=d_date,depart_time=d_time,carrier=car,travel_class=t_class)
    db.session.add(book)
    db.session.commit()
    result = {
        "Origin" : org,
        "Desrtination" : dest,
        "Departure Date" : d_date,
        "Departure Time" : d_time,
        "Carrrier" : car,
        "Travel class" : t_class,
        "Added"  : "Booking has been added!"
    }
    return jsonify({"result": result})


@app.route("/user/add_details", methods=['POST'])
def addDetails():
    origin = request.get_json()['origin']
    destination = request.get_json()['destination']
    depart_time = request.get_json()['depart_time']
    plane_name = request.get_json()['plane_name']
    travel_class = request.get_json()['travel_class']

    details = flights(origin=origin,destination=destination, depart_time=depart_time, plane_name=plane_name , travel_class=travel_class)
    db.session.add(details)
    db.session.commit()
    result = {
        "Origin" : origin,
        "Destination" : destination,
        "Depart time"  : depart_time,
        "Plane name"  : plane_name,
        "Travel class"  : travel_class,
        "Status"  : "Flights Details has been added to the list"
    }
    return jsonify({"result":result})

##cehcked
@app.route("/user/details", methods=['GET'])
def getAllDetails():
    plane =  flights.query.all()
    pl_list =[]
    for item in plane:
        d = collections.OrderedDict()
        d['origin']= item.origin
        d['destination']= item.destination
        d['depart_time']= item.depart_time
        d['carrier']= item.plane_name
        d['travel_class']= item.travel_class
        pl_list.append(d)
    details = json.dumps(pl_list)
    return details


##cecked
@app.route("/user/reserved", methods=['GET'])
def getAllReserved():
    reservedList =  booked.query.all()
    pl_list =[]
    for item in reservedList:
        d = collections.OrderedDict()
        d['origin']= item.origin
        d['destination']= item.destination
        d['depart_date']= item.depart_date.isoformat()
        d['depart_time']= item.depart_time
        d['carrier']= item.carrier
        d['travel_class']= item.travel_class
        pl_list.append(d)
    reserved = json.dumps(pl_list)
    return reserved


@app.route('/user/register', methods=['GET', 'POST'])
def register():
    usrname = request.get_json()['username']
    psswd = bcrypt.generate_password_hash(request.get_json()['password']).decode('UTF-8')
    user = Admin(username=usrname, password=psswd)
    db.session.add(user)
    db.session.commit()

    result = {
        "Username" : usrname,
        "Password" : psswd,
        "Created"  : "Admin user created!"
    }
    return jsonify({"result": result})


@app.route('/user/login', methods=['GET', 'POST'])
def login():
    usrname = request.get_json()['username']
    psswd = request.get_json()['password']
    admin = Admin.query.filter_by(username=usrname).first()
    if admin and bcrypt.check_password_hash(admin.password, psswd):
        access_token = create_access_token(identity = {'Username': usrname})
        result = jsonify({"token":access_token})
    else:
        result = jsonify({"error": "Invalid username/password!"})

    return result
